﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public enum ArcadeObjectType
{
    Bullet,
    Enemy1,
    Enemy2,
    Enemy3
}

[Serializable]
public class GameObjectBind
{
    public ArcadeObjectType Type;
    public GameObject Prefab;
    public int InitCount;
}

public class GameObjectPool : MonoBehaviour
{
    private class PoolItem
    {
        public GameObject Instance;
        public bool Using;

        public PoolItem(GameObject instance)
        {
            Instance = instance;
            Using = false;
        }
    }

    public GameObjectBind[] Prefabs;

    private Dictionary<ArcadeObjectType, List<PoolItem>> _pool = new Dictionary<ArcadeObjectType, List<PoolItem>>();

    void Start()
    {
        InitPool();
    }

    public GameObject GetFreeInstance(ArcadeObjectType objectType)
    {
        if (_pool == null || !_pool.ContainsKey(objectType)) return null;

        var poolItem = (from item in _pool[objectType] where !item.Using select item).FirstOrDefault();

        if (poolItem != null)
        {
            poolItem.Using = true;
        }

        return poolItem != null ? poolItem.Instance : null;
    }

    public void ReturnInstance(GameObject instance)
    {
        foreach (var pool in _pool)
        {
            var poolItem = (from item in pool.Value where item.Instance == instance select item).FirstOrDefault();

            if (poolItem != null)
            {
                instance.transform.position = new Vector3(1000, 1000, 1000);                
                poolItem.Using = false;

                break;
            }
        }
    }

    private void InitPool()
    {
        if (Prefabs != null && Prefabs.Length > 0)
        {
            foreach (var prefab in Prefabs)
            {
                List<PoolItem> pool = null;

                if (_pool.ContainsKey(prefab.Type))
                {
                    pool = _pool[prefab.Type];
                }
                else
                {
                    pool = new List<PoolItem>();
                    _pool.Add(prefab.Type, pool);
                }

                for (int i = 0; i < prefab.InitCount; i++)
                {
                    var instance = Instantiate(prefab.Prefab);
                    instance.transform.position = new Vector3(1000, 1000, 1000);

                    pool.Add(new PoolItem(instance));
                }
            }
        }
    }
}
