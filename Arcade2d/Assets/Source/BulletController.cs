﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour
{
    public MainGameController GameController;

    public bool Active = false;

    private float _speed = 0.3f;

    private Rigidbody2D _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Active)
        {
            _rigidbody.MovePosition(_rigidbody.position + new Vector2(_speed, 0));

            if (Camera.main.WorldToScreenPoint(_rigidbody.position).x > Screen.width)
            {
                Active = false;
                GameController.Pool.ReturnInstance(gameObject);
            }
        }
    }
}
