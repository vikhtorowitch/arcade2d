﻿using UnityEngine;
using System.Collections;

public class BoundsHelper : MonoBehaviour
{
    void OnDrawGizmos()
    {
        var renderer = GetComponent<SpriteRenderer>();
        Gizmos.DrawSphere(renderer.bounds.max, 0.2f);
    }
}
