﻿using UnityEngine;
using System.Collections;

public class HeroController : MonoBehaviour
{
    public MainGameController GameController;
    public BackgroundController Background;

    private Rigidbody2D _rigidbody;

    private Vector2 _speed = new Vector2(10, 10);
    private Vector2 _velocity;

    private void Start ()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
	}
	
	private void Update ()
    {
        float axisX = Input.GetAxis("Horizontal");
        float axisY = Input.GetAxis("Vertical");

        _velocity = new Vector2(_speed.x * axisX, _speed.y * axisY);

        if (Input.GetButtonDown("Fire1"))
        {
            GameController.MakeBullet(_rigidbody.position);
        }

    }

    private void FixedUpdate()
    {
        _rigidbody.velocity = _velocity;
    }

    public void Move(Vector2 vector)
    {
        var newPosition = _rigidbody.position + vector;

        var viewPortCoords = Camera.main.WorldToScreenPoint(newPosition);
        _rigidbody.MovePosition(newPosition);
    }
}
