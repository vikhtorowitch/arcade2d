﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundController : MonoBehaviour
{
    public SpriteRenderer SpriteRenderer;

    public SpriteRenderer[] Segments;
    public int ActiveSegmentIndex = 0;

    private int _previousSegmentIndex = 0;

    private float _speed = 0.05f;
    private float _time = 0f;

	void Update ()
    {
        MoveBackground(_speed);
	}

    public void MoveBackground(float speed)
    {
        var activeSegment = Segments[ActiveSegmentIndex];

        foreach (var seg in Segments)
        {
            var position = seg.gameObject.transform.position;
            position.x -= speed;

            seg.gameObject.transform.position = position;
        }

        var backgroundEdge = activeSegment.bounds.max;
        var backgroundExt = activeSegment.bounds.extents;
        var viewPortCoords = Camera.main.WorldToScreenPoint(backgroundEdge);

        if (Mathf.Abs(Screen.width - viewPortCoords.x) < ((float)Screen.width * 0.05f))
        {
            _previousSegmentIndex = ActiveSegmentIndex;
            ActiveSegmentIndex++;

            if (ActiveSegmentIndex > Segments.GetLength(0) - 1)
            {
                ActiveSegmentIndex = 0;
            }

            var newPosition = Segments[ActiveSegmentIndex].transform.position;
            newPosition.x = backgroundEdge.x + backgroundExt.x;

            Segments[ActiveSegmentIndex].transform.position = newPosition;
        }
    }
}
