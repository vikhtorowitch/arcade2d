﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    public MainGameController GameController;

    public bool Active = false;
    public int InitArmor;
    public int Armor;
    public int Points;
    public float Speed = 0.1f;

    private Rigidbody2D _rigidbody;

    void Start()
    { 
        _rigidbody = GetComponent<Rigidbody2D>();	
	}
	
	void Update()
    {
        if (Active)
        {
            _rigidbody.MovePosition(_rigidbody.position - new Vector2(Speed, 0));

            if (Camera.main.WorldToScreenPoint(_rigidbody.position).x < 0)
            {
                Active = false;
                GameController.Pool.ReturnInstance(gameObject);
            }
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Active)
        {
            if (collision.gameObject.tag == "Bullet")
            {
                _rigidbody.isKinematic = true;
                
                GameController.Pool.ReturnInstance(collision.gameObject);
                Armor--;

                if (Armor <= 0)
                {
                    GameController.Pool.ReturnInstance(gameObject);
                    GameController.AddPoints(Points);
                }
            }

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.SetActive(false);
                GameController.GameOver();
            }
        }
    }
}
