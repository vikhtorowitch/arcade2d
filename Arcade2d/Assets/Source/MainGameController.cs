﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainGameController : MonoBehaviour
{
    public GameObject GameOverPanel;
    public Text PointsText;

    public GameObjectPool Pool;

    public int Points = 0;

    private float _time = 0f;

	void Start()
    {
        GameOverPanel.SetActive(false);
	}
	
	void Update()
    {
        _time += Time.deltaTime;

        if (_time >= 1f)
        {
            _time = 0;

            MakeEnemy();
        }
    }

    public void MakeEnemy()
    {
        var enemyType = Mathf.RoundToInt(Random.value * 3);

        ArcadeObjectType arcadeObjectType = ArcadeObjectType.Enemy1;

        switch (enemyType)
        {
            case 1:
                arcadeObjectType = ArcadeObjectType.Enemy1;
                break;

            case 2:
                arcadeObjectType = ArcadeObjectType.Enemy2;
                break;

            case 3:
                arcadeObjectType = ArcadeObjectType.Enemy3;
                break;
        }

        var instance = Pool.GetFreeInstance(arcadeObjectType);

        if (instance != null)
        {
            instance.transform.position = new Vector3(10, -2.5f + (2.5f * Random.value), 0);

            var enemyController = instance.GetComponent<EnemyController>();

            enemyController.Armor = enemyController.InitArmor;

            enemyController.Speed = 0.02f + 0.2f * Random.value;
            enemyController.Active = true;
            enemyController.GameController = this;

            instance.GetComponent<Rigidbody2D>().isKinematic = false;
        }
    }

    public void MakeBullet(Vector3 position)
    {
        var instance = Pool.GetFreeInstance(ArcadeObjectType.Bullet);

        if (instance != null)
        {
            instance.transform.position = position + new Vector3(2f, 0, 0);
            instance.GetComponent<Rigidbody2D>().AddTorque(-1f - Random.value, ForceMode2D.Impulse);

            var bulletController = instance.GetComponent<BulletController>();

            bulletController.Active = true;
            bulletController.GameController = this;
        }
    }

    public void AddPoints(int points)
    {
        Points += points;
        PointsText.text = Points.ToString();
    }

    public void GameOver()
    {
        GameOverPanel.SetActive(true);
    }
}
